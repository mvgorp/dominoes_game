<?php

/**
 * Class tile
 *
 */
class Tile {

    /**
     * @var int
     */
    public int $nr1;
    /**
     * @var int
     */
    public int $nr2;

    /**
     * Tile constructor.
     *
     * @param int $nr1
     * @param int $nr2
     */
    public function __construct(int $nr1, int $nr2)
    {
        $this->nr1 = $nr1;
        $this->nr2 = $nr2;
    }

    /**
     * Swap the numbers
     *
     * @return $this
     */
    public function swap() : Tile
    {
        $temp = $this->nr1;
        $this->nr1 = $this->nr2;
        $this->nr2 = $temp;
        return $this;
    }

    /**
     * Return the tile in a formatted string
     *
     * @return string
     */
    public function format() : string
    {
        return '<'.$this->nr1.':'.$this->nr2.'>';
    }

}