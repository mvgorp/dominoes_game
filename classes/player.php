<?php

/**
 * Class Player
 *
 */
class Player {

    /**
     * The set of tiles a player holds
     *
     * @var TileSet
     */
    protected TileSet $deck;

    /**
     * Name of the player
     *
     * @var string
     */
    protected string $name;

    /**
     * Player constructor.
     *
     * @param TileSet $deck
     * @param $name
     */
    public function __construct(TileSet $deck, $name)
    {
        $this->deck = $deck;
        $this->name = $name;
    }

    /**
     * If the player has a tile that can be played to the board,
     * Return that tile and the position on the board where it matches
     *
     * @param TileSet $board
     * @return null|array
     */
    public function findMatchingTile(TileSet $board) : ?array
    {
        /**
         * To match, only the first and last board tiles are needed
         */
        $first = $board->getFirst();
        $last = $board->getLast();

        /**
         * @var int $i
         * @var Tile $tile
         *
         * Just find the first tile that is found to be a match
         */
        foreach($this->deck->all() as $i => $tile) {

            // Match nr1 from first tile on board
            if($first->nr1 === $tile->nr1 || $first->nr1 === $tile->nr2) {
                $this->deck->remove($i);
                return ['board_tile_position' => 'first', 'board_tile' => $first, 'play_tile' => $tile];
            }

            // Match nr2 from last tile on board
            if($last->nr2 === $tile->nr1 || $last->nr2 === $tile->nr2) {
                $this->deck->remove($i);
                return ['board_tile_position' => 'last', 'board_tile' => $last, 'play_tile' => $tile];
            }

        }

        // No matching tile found
        return null;

    }

    /**
     * Add a tile to the players deck
     *
     * @param Tile $tile
     */
    public function addTileToDeck(Tile $tile)
    {
        $this->deck->add($tile);
    }

    /**
     * Check if no tiles are left in the deck
     *
     * @return bool
     */
    public function hasNoTiles() : bool
    {
        return $this->deck->count() === 0;
    }

    /**
     * Name of the player
     *
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

}