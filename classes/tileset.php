<?php

/**
 * Class TilesSet
 *
 */
class TileSet {

    /**
     * The list of Tiles
     *
     * @var Tile[]
     */
    protected array $tiles = [];

    /**
     * Add a tile to the list of tiles on the given position:
     * - first: will add the tile at the beginning
     * - last (or any other given strng, just in case): will add the tile at the end
     *
     * @param Tile $tile
     * @param string $position
     * @return TileSet
     */
    public function add(Tile $tile, $position = 'last') : TileSet
    {
        if($position === 'first') {
            array_unshift($this->tiles, $tile);
        } else {
            array_push($this->tiles, $tile);
        }

        return $this;
    }

    /**
     * Make sure the (first and last) tile numbers align
     *
     * @return $this
     */
    public function alignTiles() : TileSet {

        if($this->count() <= 1) return $this;

        // Check the first two
        if($this->tiles[0]->nr2 !== $this->tiles[1]->nr1)
            $this->tiles[0]->swap();

        // Check the last two
        if($this->tiles[$this->count()-1]->nr1 !== $this->tiles[$this->count()-2]->nr2)
            $this->tiles[$this->count()-1]->swap();

        return $this;
    }

    /**
     * Draw a random tile from the list of tiles
     *
     * @return Tile
     */
    public function drawRandomTile() : ?Tile
    {
        // Check
        if($this->count() === 0) return null;

        // Find random
        $random_index = mt_rand(0, $this->count() - 1);
        $return_tile = $this->tiles[$random_index];

        // Remove from set
        unset($this->tiles[$random_index]);
        $this->tiles = array_values($this->tiles);

        // Return Tile
        return $return_tile;

    }

    /**
     * Draw a specific amount of random tiles
     *
     * @param int $amount
     * @return TileSet
     */
    public function drawRandomTiles($amount = 1) : TileSet
    {
        // Init
        $random_tiles = new TileSet();

        for ($i = 1; $i <= $amount; $i++)
            $random_tiles->add($this->drawRandomTile());

        return $random_tiles;

    }

    /**
     * Return the list of tiles in a space-separated string
     *
     * @return string
     */
    public function display() : string
    {
        $list = [];

        /** @var Tile $tile */
        foreach($this->tiles as $i => $tile)
            $list[] = $tile->format();

        return implode(' ', $list);
    }

    /**
     * Remove the tile on the given index
     *
     * @param $index
     * @return Tile|null
     */
    public function remove($index) : ?Tile
    {
        if(!isset($this->tiles[$index])) return null;

        // Save to return
        $tile_removed = $this->tiles[$index];

        // Remove
        unset($this->tiles[$index]);
        $this->tiles = array_values($this->tiles);

        // Return
        return $tile_removed;

    }

    /**
     * Return the full list of tiles
     *
     * @return array
     */
    public function all() : array
    {
        return $this->tiles;
    }

    /**
     * Get the first tile in the set
     *
     * @return bool|Tile
     */
    public function getFirst() : Tile
    {
        if(count($this->tiles) === 0) return false;
        return $this->tiles[0];
    }

    /**
     * Get the last tile in the set
     *
     * @return bool|Tile
     */
    public function getLast() : Tile
    {
        if(count($this->tiles) === 0) return false;

        return $this->tiles[count($this->tiles) - 1];
    }

    /**
     * What is the number of tiles in the set
     *
     * @return int
     */
    public function count() : int
    {
        return count($this->tiles);
    }

}