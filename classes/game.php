<?php

/**
 * Class DominoesGame
 *
 */
class Game {

    /**
     * Set of available tiles (face down)
     *
     * @var TileSet
     */
    protected TileSet $tileset;

    /**
     * Set of tiles on the board
     *
     * @var TileSet
     */
    protected TileSet $board;

    /**
     * Dominoes constructor.
     *
     */
    public function __construct()
    {
        $this->tileset = new TileSet();
        $this->board = new TileSet();

        $this->buildGame();
    }

    /**
     * Build all possible tiles for the dominoes game
     */
    private function buildGame()
    {
        // Build
        foreach(range(0,6) as $nr1) {
            foreach (range(0, $nr1) as $nr2) {
                $this->tileset->add(new Tile($nr1, $nr2));
            }
        }

        // Return
        return $this;
    }

    /**
     * Return 7 tiles (for a player)
     *
     * @return TileSet
     */
    public function getNewPlayerDeck() : TileSet
    {
        // Draw 7 random tiles
        return $this->tileset->drawRandomTiles(7);
    }

    /**
     * Draw a random tile from the set of tiles
     *
     * @return Tile
     */
    public function drawRandomTile() : ?Tile
    {
        return $this->tileset->drawRandomTile();
    }

    /**
     * Draw a random tile from the set of tiles
     * and add to the board
     *
     * @return Game
     */
    public function playRandomTile() : Game
    {
        $tile = $this->drawRandomTile();

        if($tile)
            $this->addToBoard($tile);

        return $this;
    }

    /**
     * Add the given tile to the board on the given position
     *
     * @param Tile $tile
     * @param null $position
     * @return $this
     */
    public function addToBoard(Tile $tile, $position = null)
    {
        $this->board->add($tile, $position)->alignTiles();

        return $this;
    }

    /**
     * Return the set of tiles that are on the board
     *
     * @return TileSet
     */
    public function getBoard()
    {
        return $this->board;
    }

    /**
     * Return the board in a formatted string
     *
     * string
     */
    public function displayBoard() : string
    {
        return $this->board->display();
    }

}
