<?php declare(strict_types=1);

/**
 * Dominoes Game
 *
 */

// Init
error_reporting(E_ALL);

// Classes
include_once './classes/tile.php';
include_once './classes/tileset.php';
include_once './classes/player.php';
include_once './classes/game.php';

// Who is playing
$player_names = ['Alice', 'Bob'];


/**
 * Prepare the Game
 *
 */
$game = new Game();

// Init players
$players = array_map(
    /** @param $name @return Player */
    function($name) use ($game) {
        return new Player($game->getNewPlayerDeck(), $name);
}, $player_names);


/**
 * Start the Game
 *
 */
$game->playRandomTile();
feedback("Game starting with first tile: {$game->displayBoard()}");


/**
 * Continue game until:
 *
 * - one of the players has no cards left
 * - No tiles are left in the Game to draw from
 */
while(true) {

    /**
     * Each player plays, one by one
     *
     * @var Player $player
     */
    foreach($players as $p => $player) {

        /**
         * Player starts playing
         */

        // Find a matching tile in the players deck
        while(!($match = $player->findMatchingTile($game->getBoard()))) {

            // No matching tile found
            feedback("{$player->getName()} can't play, ", false, false);

            // Draw new tile from the game
            if(!($new_tile = $game->drawRandomTile()))
                feedback("no tiles left in the game ...", true);

            feedback("drawing tile {$new_tile->format()}");

            $player->addTileToDeck($new_tile);

        }

        /**
         * Player plays the matching tile
         */
        feedback("{$player->getName()} plays {$match['play_tile']->format()} to connect to tile {$match['board_tile']->format()} on the board.");

        // And add matching tile to the board
        $game->addToBoard($match['play_tile'], $match['board_tile_position']);

        // Show overview of the current board
        feedback("Board is now: {$game->displayBoard()}");

        // When no tiles left, this player is the winner and game is finished!
        if($player->hasNoTiles())
            feedback("Player {$player->getName()} has won!", true);

    }

}


/**
 * Helper function to print feedback to the screen
 *
 * @param string $text
 * @param bool $br
 * @param bool $stop
 */
function feedback(string $text, $stop = false, $br = true) {
    echo $text;
    if($stop) exit;
    if($br) echo '<br><br>';
}